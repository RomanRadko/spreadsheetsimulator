package model.token;

/**
 * Token for the arithmetic operations.
 *
 * @author Roman Radko
 * @since 20.09.2015
 */
public class OperatorToken extends AbstractToken<BinaryOperation> {

    public static final String OPERATOR_PATTERN = "\\+|-|/|\\*";

    protected OperatorToken(String sourceExpression) {
        super(sourceExpression);
    }

    @Override
    protected BinaryOperation parse(String sourceExpression) {
        return BinaryOperation.fromSign(sourceExpression);
    }
}
