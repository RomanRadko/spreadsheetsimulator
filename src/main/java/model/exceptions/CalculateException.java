package model.exceptions;

/**
 * Custom exception that indicates conditions that error has been occurred during calculation process.
 *
 * @author Roman Radko
 * @since 05.10.2015
 */
public class CalculateException extends Exception {

    public CalculateException(String message) {
        super(message);
    }

    public CalculateException(String message, Throwable cause) {
        super(message, cause);
    }

    public CalculateException(Throwable cause) {
        super(cause);
    }

    protected CalculateException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
