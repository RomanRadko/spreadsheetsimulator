package model;

/**
 * Cell position {x,y} on the spreadsheet.
 *
 * @author Roman Radko
 * @since 14.09.2015
 */
public class Coordinates {

    private int xPos;

    private int yPos;

    public Coordinates(int x, int y) {
        xPos = x;
        yPos = y;
    }

    public int getX() {
        return xPos;
    }

    public int getY() {
        return yPos;
    }

    @Override
    public String toString() {
        return "{" +
                "x=" + xPos +
                ", y=" + yPos +
                '}';
    }

    @Override
    public boolean equals(Object other) {
        boolean result = false;
        if (other instanceof Coordinates) {
            Coordinates that = (Coordinates) other;
            result = (this.getX() == that.getX() && this.getY() == that.getY());
        }
        return result;
    }

    @Override public int hashCode() {
        return (31 * (31 + getX()) + getY());
    }
}
