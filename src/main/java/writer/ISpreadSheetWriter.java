package writer;

import model.Cell;
import model.Coordinates;

import java.util.Map;

/**
 * Interface which are define base method to print output spreadsheet.
 *
 * @author Roman Radko
 * @since 17.09.2015
 */
public interface ISpreadSheetWriter {

    void write(Map<Coordinates, Cell> calcRes);

}
