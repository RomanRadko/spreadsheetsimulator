package model.token;

/**
 * Token for numbers.
 *
 * @author Roman Radko
 * @since 20.09.2015
 */
public final class IntValueToken extends AbstractToken<Integer> {

    public static final String NUMBER_PATTERN = "(\\+|-)?[0-9]+";


    protected IntValueToken(String sourceExpression) {
        super(sourceExpression);
    }

    @Override
    protected Integer parse(String sourceExpression) {
        return Integer.valueOf(sourceExpression);
    }


}
