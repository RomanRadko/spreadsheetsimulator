package model.token;

/**
 * Token for the simple text.
 *
 * @author Roman Radko
 * @since 20.09.2015
 */
public class TextValueToken extends AbstractToken<String> {

    public static final String TEXT_PATTERN = "\'.+";

    protected TextValueToken(String sourceExpression) {
        super(sourceExpression);
    }

    @Override
    protected String parse(String sourceExpression) {
        return sourceExpression.substring(1);
    }
}
