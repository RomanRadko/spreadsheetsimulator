package model.token;

import model.Coordinates;

/**
 * Token for the cell references.
 *
 * @author Roman Radko
 * @since 20.09.2015
 */
public class ReferenceToken extends AbstractToken<Coordinates> {

    public static final String REFERENCE_PATTERN = "[a-zA-Z]+\\d+";

    protected ReferenceToken(String sourceExpression) {
        super(sourceExpression);
    }

    @Override
    protected Coordinates parse(String sourceExpression) {
        return new Coordinates(Integer.parseInt(sourceExpression.substring(1)) - 1, sourceExpression.toCharArray()[0] - 'A');
    }
}
