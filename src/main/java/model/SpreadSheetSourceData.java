package model;

import java.util.Map;

/**
 * Entity which are representing spreadsheet with cells.
 *
 * @author Roman Radko
 * @since 14.09.2015
 */
public class SpreadSheetSourceData {

    private Map<Coordinates, String> mCellsSourceData;

    private int mRowCount;

    private int mColumnCount;

    public SpreadSheetSourceData(int rowCount, int columnCount, Map<Coordinates, String> cells) {
        mRowCount = rowCount;
        mColumnCount = columnCount;
        mCellsSourceData = cells;
    }

    public Map<Coordinates, String> getCells() {
        return mCellsSourceData;
    }
}
