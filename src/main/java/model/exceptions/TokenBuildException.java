package model.exceptions;

/**
 * Custom exception that indicates conditions that error has been occurred during building token.
 *
 * @author Roman Radko
 * @since 20.09.2015
 */
public class TokenBuildException extends Exception {

    public TokenBuildException() {
        super();
    }

    public TokenBuildException(String message) {
        super(message);
    }

    public TokenBuildException(String message, Throwable cause) {
        super(message, cause);
    }

    public TokenBuildException(Throwable cause) {
        super(cause);
    }
}
