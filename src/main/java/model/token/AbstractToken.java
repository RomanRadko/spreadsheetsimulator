package model.token;

import model.exceptions.TokenBuildException;

import java.util.regex.Pattern;

/**
 * Basic class for tokens.
 *
 * @author Roman Radko
 * @since 20.09.2015
 * @param <T> generic type
 */
public abstract class AbstractToken <T> {

    private final T mParsedValue;

    private final String mSourceExpression;

    protected AbstractToken(String sourceExpression) {
        mSourceExpression = sourceExpression;
        mParsedValue = parse(sourceExpression);
    }

    // Static factory which creates particular implementation of AbstractToken
    public static AbstractToken<?> from(String sourceExpression) throws TokenBuildException {
        if (Pattern.compile(IntValueToken.NUMBER_PATTERN).matcher(sourceExpression).matches()) {
            return new IntValueToken(sourceExpression);
        } else if (Pattern.compile(TextValueToken.TEXT_PATTERN).matcher(sourceExpression).matches()) {
            return new TextValueToken(sourceExpression);
        } else if (Pattern.compile(OperatorToken.OPERATOR_PATTERN).matcher(sourceExpression).matches()) {
            return new OperatorToken(sourceExpression);
        } else if (Pattern.compile(ReferenceToken.REFERENCE_PATTERN).matcher(sourceExpression).matches()) {
            return new ReferenceToken(sourceExpression);
        } else {
            throw new TokenBuildException("Unknown type [" + sourceExpression + "}");
        }
    }

    protected abstract T parse(String sourceExpression);

    public T getParsedValue() {
        return mParsedValue;
    }

    @Override
    public String toString() {
        return "AbstractToken{" +
                "parsedValue=" + mParsedValue +
                ", sourceExpression='" + mSourceExpression + '\'' +
                '}';
    }
}