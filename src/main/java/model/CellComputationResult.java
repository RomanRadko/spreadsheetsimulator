package model;

/**
 * Aimed for store result of computation after parsing.
 *
 * @author Roman Radko
 * @since 20.09.2015
 */
public class CellComputationResult {

    private String mResult;

    public CellComputationResult(String result) {
        mResult = result;
    }

    @Override
    public String toString() {
        return mResult;
    }
}
