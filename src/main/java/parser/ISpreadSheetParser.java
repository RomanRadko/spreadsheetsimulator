package parser;

import model.exceptions.SyntaxParserException;
import model.token.AbstractToken;

import java.util.List;

/**
 * Convert cell content into the grammar elements.
 *
 * @author Roman Radko
 * @since 16.09.2015
 */
public interface ISpreadSheetParser {

    List<AbstractToken<?>> parse(String sourceExpression) throws SyntaxParserException;
}
