package model.token;

/**
 * Represents possible arithmetic operations.
 *
 * @author Roman Radko
 * @since 20.09.2015
 */
public enum BinaryOperation {

    Plus("+") {
        @Override
        public int apply(int operand1, int operand2) {
            return operand1 + operand2;
        }
    },

    Minus("-") {
        @Override
        public int apply(int operand1, int operand2) {
            return operand1 - operand2;
        }
    },

    Mult("*") {
        @Override
        public int apply(int operand1, int operand2) {
            return operand1 * operand2;
        }
    },

    Div("/") {
        @Override
        public int apply(int operand1, int operand2) {
            return operand1 / operand2;
        }
    },;

    private final String operationSign;

    BinaryOperation(String operationSign) {
        this.operationSign = operationSign;
    }

    public static BinaryOperation fromSign(String operatorSign) {
        for (BinaryOperation operation : values()) {
            if (operation.operationSign.equals(operatorSign)) {
                return operation;
            }
        }
        return null;
    }

    public abstract int apply(int operand1, int operand2);
}