package model;

import model.token.AbstractToken;
import model.token.IntValueToken;
import model.token.TextValueToken;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Comparator;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Entity which are represents cell - spread sheet unit.
 *
 * @author Roman Radko
 * @since 14.09.2015
 */
public class Cell {

    private static final Logger LOGGER = LogManager.getLogger(Cell.class);

    public static final String EXPRESSION_PATTERN = "^=.*$";

    /**
     * Possible types.
     */
    public enum Type {
        NOTHING, EXPRESSION, NUMBER, TEXT, UNKNOWN;

        public static Type from(String expr) {
            Pattern textPattern = Pattern.compile(TextValueToken.TEXT_PATTERN);
            Pattern numberPattern = Pattern.compile(IntValueToken.NUMBER_PATTERN);
            Pattern expressionPattern = Pattern.compile(EXPRESSION_PATTERN);
            if (textPattern.matcher(expr).matches()) {
                //LOGGER.error("expr: " + expr + ", type = " + Type.TEXT);
                return Type.TEXT;
            } else if (numberPattern.matcher(expr).matches()) {
                //LOGGER.error("expr: " + expr + ", type = " + Type.NUMBER);
                return Type.NUMBER;
            } else if (expressionPattern.matcher(expr).matches()) {
                //LOGGER.error("expr: " + expr + ", type = " + Type.EXPRESSION);
                return Type.EXPRESSION;
            } else if (expr.isEmpty()) {
                //LOGGER.error("expr: " + expr + ", type = " + Type.NOTHING);
                return Type.NOTHING;
            } else {
                //LOGGER.error("expr: " + expr + ", type = " + Type.UNKNOWN);
                return Type.UNKNOWN;
            }
        }
    }

    /**
     * Type of possible cell content.
     */
    private Type mType;
    /**
     * List with unary tokens which stored inside.
     */
    private List<AbstractToken<?>> mContent;
    /**
     * Position {x,y} on the spreadsheet.
     */
    private Coordinates mCoordinates;

    private CellComputationResult result;

    private boolean isCalculated;

    public Cell(List<AbstractToken<?>> content, Type type, Coordinates coordinates) {
        mContent = content;
        mType = type;
        mCoordinates = coordinates;
        result = null;
    }

    public Type getType() {
        return mType;
    }

    public void setType(Type type) {
        mType = type;
    }

    public List<AbstractToken<?>> getContent() {
        return mContent;
    }

    public void setContent(List<AbstractToken<?>> content) {
        mContent = content;
    }

    public void setResult(CellComputationResult result) {
        this.result = result;
    }

    public Coordinates getCoordinates() {
        return mCoordinates;
    }

    public boolean isCalculated() {
        return isCalculated;
    }

    public void setIsCalculated(boolean isCalculated) {
        this.isCalculated = isCalculated;
    }

    public CellComputationResult getResult() {
        return result;
    }

    @Override
    public String toString() {
        return "Cell{" +
                "mType=" + mType +
                ", mContent=" + mContent +
                ", mCoordinates=" + mCoordinates +
                ", result=" + result +
                '}';
    }
}

