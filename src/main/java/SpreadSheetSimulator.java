import model.Cell;
import model.CellComputationResult;
import model.Coordinates;
import model.SpreadSheetSourceData;
import model.exceptions.CalculateException;
import model.exceptions.SyntaxParserException;
import model.exceptions.TokenBuildException;
import model.token.AbstractToken;
import model.token.BinaryOperation;
import model.token.IntValueToken;
import model.token.OperatorToken;
import model.token.ReferenceToken;
import model.token.TextValueToken;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import parser.SpreadSheetParser;
import reader.SpreadSheetReader;
import writer.SpreadSheetWriter;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class SpreadSheetSimulator {

    private static final Logger LOGGER = LogManager.getLogger(SpreadSheetSimulator.class);

    public static void main(String[] args) {
        if (args.length < 1) {
            LOGGER.error("Error, usage: java ClassName inputfile");
            System.exit(1);
        }
        SpreadSheetSimulator simulator = new SpreadSheetSimulator();
        try {
            Long startTime = System.currentTimeMillis();
            System.out.println("Start cell processing...");
            System.out.println("/-----------------------------------/");
            File file = new File(args[0]);
            Scanner scanner = new Scanner(file);

            // Read source data abstraction level STRING
            SpreadSheetReader reader = new SpreadSheetReader(scanner);
            SpreadSheetSourceData readResult = reader.read();

            // Parse read data
            SpreadSheetParser parser = new SpreadSheetParser();
            Map<Coordinates, Cell> parsedData = simulator.parseSourceData(parser, readResult);

            // Calculation
            Map<Coordinates, Cell> calcRes = null;
            try {
                calcRes = simulator.calculate(parsedData);
            } catch (TokenBuildException e) {
                e.printStackTrace();
            }
            //LOGGER.error("After calculation: " + calcRes);
            SpreadSheetWriter resultWriter = new SpreadSheetWriter();
            resultWriter.write(calcRes);
            System.out.println("/-----------------------------------/");
            System.out.println("End cell processing: " + (System.currentTimeMillis() - startTime) + "ms");

        } catch (FileNotFoundException | SyntaxParserException e) {
            e.printStackTrace();
        }
    }

    private Map<Coordinates, Cell> parseSourceData(SpreadSheetParser parser, SpreadSheetSourceData readResult) throws SyntaxParserException {
        Map<Coordinates, Cell> resMap = new HashMap<>();
        for (Map.Entry<Coordinates, String> readEntry : readResult.getCells().entrySet()) {
            // Rid of '='
            String formExpr = readEntry.getValue().replaceAll("[=]", "");
            List<AbstractToken<?>> cellParseRes = parser.parse(formExpr);
            Cell cell = new Cell(cellParseRes, Cell.Type.from(readEntry.getValue()), readEntry.getKey());
            resMap.put(readEntry.getKey(), cell);
        }
        //LOGGER.error("parseSourceData, resMap.values: " + resMap.values());
        return resMap;
    }

    private Map<Coordinates, Cell> calculate(Map<Coordinates, Cell> parsedData) throws TokenBuildException {
        for (Cell cell : parsedData.values()) {
            if (cell.getResult() == null) {
                try {
                    cell.setResult(new CellComputationResult(proceedTokens(cell.getContent(), parsedData)));
                } catch (CalculateException e) {
                    cell.setResult(new CellComputationResult("#ERROR"));
                }
            }
        }
        return parsedData;
    }

    private String proceedTokens(List<AbstractToken<?>> tokens, Map<Coordinates, Cell> parsedData) throws CalculateException {
        LinkedList<AbstractToken> calculationStack = new LinkedList<>();
        // Stack for Operators
        LinkedList<BinaryOperation> operators = new LinkedList<>();
        int result = 0;
        try {
            for (AbstractToken<?> token : tokens) {
                if (token instanceof IntValueToken) {
                    calculationStack.add(token);
                } else if (token instanceof OperatorToken) {
                    operators.add((BinaryOperation) token.getParsedValue());
                } else if (token instanceof TextValueToken) {
                    return (String) token.getParsedValue();
                } else if (token instanceof ReferenceToken) {
                    String res;
                    if (parsedData.get(token.getParsedValue()).getResult() != null) {
                        res = parsedData.get(token.getParsedValue()).getResult().toString();
                    } else {
                        res = proceedTokens(parsedData.get(token.getParsedValue()).getContent(), parsedData);
                    }
                    try {
                        calculationStack.add(AbstractToken.from(res));
                    } catch (NumberFormatException | TokenBuildException e) {
                        throw new CalculateException(e);
                    }
                }
            }

            while (calculationStack.size() > 1) {
                //add firstOperand and secondOperand type checking and throwing of calculation exception or return string ERROR
                AbstractToken<Integer> firstOperand = calculationStack.removeFirst();
                AbstractToken<Integer> secondOperand = calculationStack.removeFirst();
                calculationStack.addFirst(AbstractToken.from(Integer.toString(operators.removeFirst().apply(firstOperand.getParsedValue(), secondOperand.getParsedValue()))));
            }
            result = (Integer)calculationStack.removeFirst().getParsedValue();
        } catch (CalculateException ex) {
            throw new CalculateException(ex);
        } catch (TokenBuildException e) {
            e.printStackTrace();
        }
        return String.valueOf(result);
    }
}
