package reader;

import model.Coordinates;
import model.SpreadSheetSourceData;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Performs reading of the spreadsheet.
 *
 * @author Roman Radko
 * @since 14.09.2015
 */
public class SpreadSheetReader {

    private static final Logger LOGGER = LogManager.getLogger(SpreadSheetReader.class);

    private Scanner mScanner;

    public SpreadSheetReader(Scanner scanner) {
        mScanner = scanner;
    }

    public SpreadSheetSourceData read() {
        String firstLine = mScanner.nextLine();
        String[] size = firstLine.split(" ");
        int rowCount = Integer.parseInt(size[0]);
        int columnCount = Integer.parseInt(size[1]);
        //LOGGER.error("rowCount[ " + rowCount + "],  columnCount[" + columnCount + "]");
        return new SpreadSheetSourceData(rowCount, columnCount, readCells(columnCount, columnCount * rowCount));
    }

    private Map<Coordinates, String> readCells(int columnCount, int cellCount) {
        Map<Coordinates, String> cellsMap = new HashMap<>();
        for (int i = 0; i < cellCount; i++) {
            String content = mScanner.useDelimiter("\\s+").next();
            cellsMap.put(new Coordinates(i/columnCount , i % columnCount), content);
            //LOGGER.error("content" + content);
        }
        //LOGGER.error("cells" + cellsMap);
        return cellsMap;
    }

}
