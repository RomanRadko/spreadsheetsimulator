package writer;

import model.Cell;
import model.CellComparator;
import model.Coordinates;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Map;
import java.util.TreeMap;

/**
 * Aimed to print converted spreadsheet.
 *
 * @author Roman Radko
 * @since 17.09.2015
 */
public class SpreadSheetWriter implements ISpreadSheetWriter {

    private static final Logger LOGGER = LogManager.getLogger(SpreadSheetWriter.class);

    @Override
    public void write(Map<Coordinates, Cell> calcRes) {
        StringBuilder builder = new StringBuilder();
        int x = 0;
        Map<Coordinates, Cell> treeMap = new TreeMap<>(new CellComparator());
        treeMap.putAll(calcRes);
        for (Cell cell : treeMap.values()) {
            builder.append((x == cell.getCoordinates().getX()) ? cell.getResult(): "\n" + cell.getResult());
            builder.append("\t");
            x = cell.getCoordinates().getX();
        }
        System.out.println(builder.toString());
    }

}
