package model;

import java.util.Comparator;

/**
 * Compares cell's coordinates.
 *
 * @author Roman Radko
 * @since 06.10.2015
 */
public class CellComparator implements Comparator<Coordinates> {
    @Override
    public int compare(Coordinates cell1, Coordinates cell2) {
        if (cell1.getX() == cell2.getX()) {
            return cell1.getY() - cell2.getY();
        } else {
            return cell1.getX() - cell2.getX();
        }
    }
}
