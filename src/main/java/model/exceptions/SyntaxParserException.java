package model.exceptions;

/**
 * Custom exception that indicates conditions that error has been occurred during expression parsing.
 *
 * @author Roman Radko
 * @since 21.09.2015
 */
public class SyntaxParserException extends Exception {

    public SyntaxParserException(String message) {
        super(message);
    }

    public SyntaxParserException(String message, Throwable cause) {
        super(message, cause);
    }

    public SyntaxParserException(Throwable cause) {
        super(cause);
    }

    protected SyntaxParserException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
