package parser;

import model.exceptions.SyntaxParserException;
import model.exceptions.TokenBuildException;
import model.token.AbstractToken;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * Performs cells parsing.
 *
 * @author Roman Radko
 * @since 16.09.2015
 */
public class SpreadSheetParser implements ISpreadSheetParser {

    private static final Logger LOGGER = LogManager.getLogger(SpreadSheetParser.class);

    public SpreadSheetParser() {
    }

    @Override
    public List<AbstractToken<?>> parse(String sourceExpression) throws SyntaxParserException {
        //LOGGER.error("Start parsing...");
        List<String> concatList = new ArrayList<>();
        StringTokenizer tokenizer = new StringTokenizer(sourceExpression, "+-*/", true);
        while (tokenizer.hasMoreTokens()) {
            concatList.add(tokenizer.nextToken());
        }
        //LOGGER.error("Concat list: " + concatList);
        List<AbstractToken<?>> result = new ArrayList<>();
        for (String expr : concatList) {
            try {
                result.add(AbstractToken.from(expr));
            } catch (TokenBuildException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

}
